package com.cocus.travel.TravelApi;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.time.Instant;
import java.util.Date;
import java.util.LinkedList;
import java.util.Map;
import java.util.OptionalDouble;
import java.sql.*;

public class Flights {
    JsonObject flightsJson;
    Statement statement;

    public Flights() {
        try{
            Connection dbConn = DriverManager.getConnection("jdbc:mysql://localhost:3306/Cocus?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC", "root", "");
            this.statement = dbConn.createStatement();
        }catch(SQLException e){
            System.out.println("DB connection problem");
            e.printStackTrace();
        }
    }

    public void getApiJson(String stringUrl) {
        //String builder for appending the many lines of the json returned
        StringBuilder result = new StringBuilder();

        try {
            //Create URL object with string argument
            URL url = new URL(stringUrl);
            //Make connection to the desired URL
            HttpURLConnection conn = (HttpURLConnection)url.openConnection();
            //Define http operation as GET
            conn.setRequestMethod("GET");
            //Read the json available at the end point
            BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            //Temporary variable for reading which line value
            String line;
            //While there are lines available, append them to the result
            while((line = reader.readLine()) != null) {
                result.append(line);
            }
            //Convert the string with all the info into a json
            this.flightsJson = (new JsonParser()).parse(result.toString()).getAsJsonObject();

        } catch (IOException e) {
            System.out.println("Connection to the API failed");
            e.printStackTrace();
        }

    }

    public JsonObject getFlightsWithCity(String dCity){
        //Return json
        JsonObject results = new JsonObject();
        //ArrayList for storing the price flights. The values verified were int but the average will have a floating point therefore is declared double
        LinkedList<Double> priceFlights = new LinkedList<>();
        //ArrayList for storing the one bag prices. The values verified were int but the average will have a floating point therefore is declared double
        LinkedList<Double> oneBagPrice = new LinkedList<>();
        //ArrayList for storing the two bag prices. The values verified were int but the average will have a floating point therefore is declared double
        LinkedList<Double> twoBagPrice = new LinkedList<>();
        //Variable responsible for start date
        Date startDate = null;
        //Variable responsible for finish date
        Date endDate = null;

        //Verify if user is searching OPO->LIS or LIS->OPO
        if(dCity.equals("OPO")){
            //Get OPO->LIS information
            this.getApiJson("https://api.skypicker.com/flights?flyFrom=" + dCity+ "&to=LIS&partner=picky&curr=EUR");
        }else if(dCity.equals("LIS")){
            //Get LIS->OPO information
            this.getApiJson("https://api.skypicker.com/flights?flyFrom=" + dCity+ "&to=OPO&partner=picky&curr=EUR");
        }

        //Loop through all the entries in the flights json
        for(Map.Entry<String, JsonElement> entry: flightsJson.entrySet()){
            //Verify if the currency is Euro. Otherwise it breaks
            if(entry.getKey().equals("currency") && !entry.getValue().getAsString().equals("EUR")){
                break;
            }
            //Verify if the entry in the api results is the data
            if(entry.getKey().equals("data")){
                //Loop through the flights in the data
                for(JsonElement dataEntry: entry.getValue().getAsJsonArray()){
                    //This verifies if the airlines are always tap or ryanair not allowing other airlines
                    if(dataEntry.getAsJsonObject().get("airlines").getAsJsonArray().get(0).getAsString().equals("TP") || dataEntry.getAsJsonObject().get("airlines").getAsJsonArray().get(0).getAsString().equals("FR")) {
                        //verify if the value is not null (not common but might happen)
                        if(dataEntry.getAsJsonObject().get("price") != null) {
                            //Add the price to the arraylist of prices
                            priceFlights.add(dataEntry.getAsJsonObject().get("price").getAsDouble());
                        }
                        //If the price is equivalent to null then it doesn't add to the linkedlist
                        if (dataEntry.getAsJsonObject().get("bags_price").getAsJsonObject().get("1") != null) {
                            oneBagPrice.add(dataEntry.getAsJsonObject().get("bags_price").getAsJsonObject().get("1").getAsDouble());
                        }
                        //If the price is equivalent to null then it doesn't add to the linkedlist
                        if (dataEntry.getAsJsonObject().get("bags_price").getAsJsonObject().get("2") != null) {
                            twoBagPrice.add(dataEntry.getAsJsonObject().get("bags_price").getAsJsonObject().get("2").getAsDouble());
                        }
                    }
                    //Verify the dates and adjust properly
                    if(startDate == null && endDate == null){
                        //Convert the epochs to instant. Store the value in temp variable
                        Instant tempDateTime = Instant.ofEpochSecond(dataEntry.getAsJsonObject().get("dTime").getAsLong());
                        //Add the date to the start and end dates from instant.
                        startDate = Date.from(tempDateTime);
                        endDate = Date.from(tempDateTime);
                    }else{
                        //create temp variable to contain the date
                        Instant tempDateTime = Instant.ofEpochSecond(dataEntry.getAsJsonObject().get("dTime").getAsLong());
                        //Verify if the date is before the startDate
                        if(Date.from(tempDateTime).before(startDate)){
                            startDate = Date.from(tempDateTime);
                        //Verify if the date is after the endDate
                        }else if(Date.from(tempDateTime).after(endDate)){
                            endDate = Date.from(tempDateTime);
                        }
                    }
                }
            }
        }
        //Streams were used for simplicity and better performance. As to be an optionalDouble since it might be empty. Calculates prices average
        OptionalDouble averageFlightPrice = priceFlights.stream().mapToDouble(v -> v).average();
        //Streams were used for simplicity and better performance. As to be an optionalDouble since it might be empty. Calculates prices average
        OptionalDouble averageOneBagPrice = oneBagPrice.stream().mapToDouble(v -> v).average();
        //Streams were used for simplicity and better performance. As to be an optionalDouble since it might be empty. Calculates prices average
        OptionalDouble averageTwoBagsPrice = twoBagPrice.stream().mapToDouble(v -> v).average();

        //Add the averageFlightPrice to results json
        if(averageFlightPrice.isPresent()){
            results.addProperty("AverageFlightPrice", averageFlightPrice.getAsDouble());
        }else{
            results.addProperty("AverageFlightPrice", 0);
        }

        //Add the averageOneBagPrice to results json
        if(averageOneBagPrice.isPresent()){
            results.addProperty("OneBagPrice", averageOneBagPrice.getAsDouble());
        }else{
            results.addProperty("OneBagPrice", 0);
        }

        //Add the averageTwoBagPrice to results json
        if(averageTwoBagsPrice.isPresent()){
            results.addProperty("TwoBagsPrice", averageOneBagPrice.getAsDouble());
        }else{
            results.addProperty("TwoBagsPrice", 0);
        }

        //Add currency
        results.addProperty("currency", "EUR");

        //Add From Date
        results.addProperty("From", startDate.toString());

        //Add To Date
        results.addProperty("To", endDate.toString());

        //add record to the db
        addRecordToDb(results);

        //return the json with the results
        return results;
    }

    public JsonObject getFlightsWithTime(String dCity, String fDate, String tDate){
        //Return json
        JsonObject results = new JsonObject();
        //ArrayList for storing the price flights. The values verified were int but the average will have a floating point therefore is declared double
        LinkedList<Double> priceFlights = new LinkedList<>();
        //ArrayList for storing the one bag prices. The values verified were int but the average will have a floating point therefore is declared double
        LinkedList<Double> oneBagPrice = new LinkedList<>();
        //ArrayList for storing the two bag prices. The values verified were int but the average will have a floating point therefore is declared double
        LinkedList<Double> twoBagPrice = new LinkedList<>();
        //Variable responsible for start date
        Date startDate = null;
        //Variable responsible for finish date
        Date endDate = null;

        //Verify if user is searching OPO->LIS or LIS->OPO
        if(dCity.equals("OPO")){
            //Get OPO->LIS information
            this.getApiJson("https://api.skypicker.com/flights?flyFrom=" + dCity+ "&to=LIS&dateFrom=" + fDate + "&dateTo=" + tDate + "&partner=picky&curr=EUR");
        }else if(dCity.equals("LIS")){
            //Get LIS->OPO information
            this.getApiJson("https://api.skypicker.com/flights?flyFrom=" + dCity+ "&to=OPO&dateFrom=" + fDate + "&dateTo=" + tDate + "&partner=picky&curr=EUR");
        }

        //Loop through all the entries in the flights json
        for(Map.Entry<String, JsonElement> entry: flightsJson.entrySet()){
            //Verify if the currency is Euro. Otherwise it breaks
            if(entry.getKey().equals("currency") && !entry.getValue().getAsString().equals("EUR")){
                break;
            }
            //Verify if the entry in the api results is the data
            if(entry.getKey().equals("data")){
                //Loop through the flights in the data
                for(JsonElement dataEntry: entry.getValue().getAsJsonArray()){
                    //This verifies if the airlines are always tap or ryanair not allowing other airlines
                    if(dataEntry.getAsJsonObject().get("airlines").getAsJsonArray().get(0).getAsString().equals("TP") || dataEntry.getAsJsonObject().get("airlines").getAsJsonArray().get(0).getAsString().equals("FR")) {
                        //verify if the value is not null (not common but might happen)
                        if(dataEntry.getAsJsonObject().get("price") != null) {
                            //Add the price to the arraylist of prices
                            priceFlights.add(dataEntry.getAsJsonObject().get("price").getAsDouble());
                        }
                        //If the price is equivalent to null then it doesn't add to the linkedlist
                        if (dataEntry.getAsJsonObject().get("bags_price").getAsJsonObject().get("1") != null) {
                            oneBagPrice.add(dataEntry.getAsJsonObject().get("bags_price").getAsJsonObject().get("1").getAsDouble());
                        }
                        //If the price is equivalent to null then it doesn't add to the linkedlist
                        if (dataEntry.getAsJsonObject().get("bags_price").getAsJsonObject().get("2") != null) {
                            twoBagPrice.add(dataEntry.getAsJsonObject().get("bags_price").getAsJsonObject().get("2").getAsDouble());
                        }
                    }
                    //Verify the dates and adjust properly
                    if(startDate == null && endDate == null){
                        //Convert the epochs to instant. Store the value in temp variable
                        Instant tempDateTime = Instant.ofEpochSecond(dataEntry.getAsJsonObject().get("dTime").getAsLong());
                        //Add the date to the start and end dates from instant.
                        startDate = Date.from(tempDateTime);
                        endDate = Date.from(tempDateTime);
                    }else{
                        //create temp variable to contain the date
                        Instant tempDateTime = Instant.ofEpochSecond(dataEntry.getAsJsonObject().get("dTime").getAsLong());
                        //Verify if the date is before the startDate
                        if(Date.from(tempDateTime).before(startDate)){
                            startDate = Date.from(tempDateTime);
                            //Verify if the date is after the endDate
                        }else if(Date.from(tempDateTime).after(endDate)){
                            endDate = Date.from(tempDateTime);
                        }
                    }
                }
            }
        }
        //Streams were used for simplicity and better performance. As to be an optionalDouble since it might be empty. Calculates prices average
        OptionalDouble averageFlightPrice = priceFlights.stream().mapToDouble(v -> v).average();
        //Streams were used for simplicity and better performance. As to be an optionalDouble since it might be empty. Calculates prices average
        OptionalDouble averageOneBagPrice = oneBagPrice.stream().mapToDouble(v -> v).average();
        //Streams were used for simplicity and better performance. As to be an optionalDouble since it might be empty. Calculates prices average
        OptionalDouble averageTwoBagsPrice = twoBagPrice.stream().mapToDouble(v -> v).average();

        //Add the averageFlightPrice to results json
        if(averageFlightPrice.isPresent()){
            results.addProperty("AverageFlightPrice", averageFlightPrice.getAsDouble());
        }else{
            results.addProperty("AverageFlightPrice", 0);
        }

        //Add the averageOneBagPrice to results json
        if(averageOneBagPrice.isPresent()){
            results.addProperty("OneBagPrice", averageOneBagPrice.getAsDouble());
        }else{
            results.addProperty("OneBagPrice", 0);
        }

        //Add the averageTwoBagPrice to results json
        if(averageTwoBagsPrice.isPresent()){
            results.addProperty("TwoBagsPrice", averageOneBagPrice.getAsDouble());
        }else{
            results.addProperty("TwoBagsPrice", 0);
        }

        //Add currency
        results.addProperty("currency", "EUR");

        //Add From Date
        results.addProperty("From", startDate.toString());

        //Add To Date
        results.addProperty("To", endDate.toString());

        //add record to the db
        addRecordToDb(results);

        //return the json with the results
        return results;
    }

    public JsonObject getFlightsWithCurrency(String dCity, String currency){
        //Return json
        JsonObject results = new JsonObject();
        //ArrayList for storing the price flights. The values verified were int but the average will have a floating point therefore is declared double
        LinkedList<Double> priceFlights = new LinkedList<>();
        //ArrayList for storing the one bag prices. The values verified were int but the average will have a floating point therefore is declared double
        LinkedList<Double> oneBagPrice = new LinkedList<>();
        //ArrayList for storing the two bag prices. The values verified were int but the average will have a floating point therefore is declared double
        LinkedList<Double> twoBagPrice = new LinkedList<>();
        //Variable responsible for start date
        Date startDate = null;
        //Variable responsible for finish date
        Date endDate = null;

        //Verify if user is searching OPO->LIS or LIS->OPO
        if(dCity.equals("OPO")){
            //Get OPO->LIS information
            this.getApiJson("https://api.skypicker.com/flights?flyFrom=" + dCity+ "&to=LIS&partner=picky&curr=" + currency);
        }else if(dCity.equals("LIS")){
            //Get LIS->OPO information
            this.getApiJson("https://api.skypicker.com/flights?flyFrom=" + dCity+ "&to=OPO&partner=picky&curr=");
        }

        //Loop through all the entries in the flights json
        for(Map.Entry<String, JsonElement> entry: flightsJson.entrySet()){
            //Verify if the currency is Euro. Otherwise it breaks
            if(entry.getKey().equals("currency") && !entry.getValue().getAsString().equals("EUR")){
                break;
            }
            //Verify if the entry in the api results is the data
            if(entry.getKey().equals("data")){
                //Loop through the flights in the data
                for(JsonElement dataEntry: entry.getValue().getAsJsonArray()){
                    //This verifies if the airlines are always tap or ryanair not allowing other airlines
                    if(dataEntry.getAsJsonObject().get("airlines").getAsJsonArray().get(0).getAsString().equals("TP") || dataEntry.getAsJsonObject().get("airlines").getAsJsonArray().get(0).getAsString().equals("FR")) {
                        //verify if the value is not null (not common but might happen)
                        if(dataEntry.getAsJsonObject().get("price") != null) {
                            //Add the price to the arraylist of prices
                            priceFlights.add(dataEntry.getAsJsonObject().get("price").getAsDouble());
                        }
                        //If the price is equivalent to null then it doesn't add to the linkedlist
                        if (dataEntry.getAsJsonObject().get("bags_price").getAsJsonObject().get("1") != null) {
                            oneBagPrice.add(dataEntry.getAsJsonObject().get("bags_price").getAsJsonObject().get("1").getAsDouble());
                        }
                        //If the price is equivalent to null then it doesn't add to the linkedlist
                        if (dataEntry.getAsJsonObject().get("bags_price").getAsJsonObject().get("2") != null) {
                            twoBagPrice.add(dataEntry.getAsJsonObject().get("bags_price").getAsJsonObject().get("2").getAsDouble());
                        }
                    }
                    //Verify the dates and adjust properly
                    if(startDate == null && endDate == null){
                        //Convert the epochs to instant. Store the value in temp variable
                        Instant tempDateTime = Instant.ofEpochSecond(dataEntry.getAsJsonObject().get("dTime").getAsLong());
                        //Add the date to the start and end dates from instant.
                        startDate = Date.from(tempDateTime);
                        endDate = Date.from(tempDateTime);
                    }else{
                        //create temp variable to contain the date
                        Instant tempDateTime = Instant.ofEpochSecond(dataEntry.getAsJsonObject().get("dTime").getAsLong());
                        //Verify if the date is before the startDate
                        if(Date.from(tempDateTime).before(startDate)){
                            startDate = Date.from(tempDateTime);
                            //Verify if the date is after the endDate
                        }else if(Date.from(tempDateTime).after(endDate)){
                            endDate = Date.from(tempDateTime);
                        }
                    }
                }
            }
        }
        //Streams were used for simplicity and better performance. As to be an optionalDouble since it might be empty. Calculates prices average
        OptionalDouble averageFlightPrice = priceFlights.stream().mapToDouble(v -> v).average();
        //Streams were used for simplicity and better performance. As to be an optionalDouble since it might be empty. Calculates prices average
        OptionalDouble averageOneBagPrice = oneBagPrice.stream().mapToDouble(v -> v).average();
        //Streams were used for simplicity and better performance. As to be an optionalDouble since it might be empty. Calculates prices average
        OptionalDouble averageTwoBagsPrice = twoBagPrice.stream().mapToDouble(v -> v).average();

        //Add the averageFlightPrice to results json
        if(averageFlightPrice.isPresent()){
            results.addProperty("AverageFlightPrice", averageFlightPrice.getAsDouble());
        }else{
            results.addProperty("AverageFlightPrice", 0);
        }

        //Add the averageOneBagPrice to results json
        if(averageOneBagPrice.isPresent()){
            results.addProperty("OneBagPrice", averageOneBagPrice.getAsDouble());
        }else{
            results.addProperty("OneBagPrice", 0);
        }

        //Add the averageTwoBagPrice to results json
        if(averageTwoBagsPrice.isPresent()){
            results.addProperty("TwoBagsPrice", averageOneBagPrice.getAsDouble());
        }else{
            results.addProperty("TwoBagsPrice", 0);
        }

        //Add currency
        results.addProperty("currency", currency);

        //Add From Date
        results.addProperty("From", startDate.toString());

        //Add To Date
        results.addProperty("To", endDate.toString());

        //add record to the db
        addRecordToDb(results);

        //return the json with the results
        return results;
    }

    public JsonObject getFlightsWithCurrencyAndDate(String dCity, String currency, String fDate, String tDate){
        //Return json
        JsonObject results = new JsonObject();
        //ArrayList for storing the price flights. The values verified were int but the average will have a floating point therefore is declared double
        LinkedList<Double> priceFlights = new LinkedList<>();
        //ArrayList for storing the one bag prices. The values verified were int but the average will have a floating point therefore is declared double
        LinkedList<Double> oneBagPrice = new LinkedList<>();
        //ArrayList for storing the two bag prices. The values verified were int but the average will have a floating point therefore is declared double
        LinkedList<Double> twoBagPrice = new LinkedList<>();
        //Variable responsible for start date
        Date startDate = null;
        //Variable responsible for finish date
        Date endDate = null;

        //Verify if user is searching OPO->LIS or LIS->OPO
        if(dCity.equals("OPO")){
            //Get OPO->LIS information
            this.getApiJson("https://api.skypicker.com/flights?flyFrom=" + dCity+ "&to=LIS&dateFrom=" + fDate + "&dateTo=" + tDate + "&partner=picky&curr=" + currency);
        }else if(dCity.equals("LIS")){
            //Get LIS->OPO information
            this.getApiJson("https://api.skypicker.com/flights?flyFrom=" + dCity+ "&to=OPO&dateFrom=" + fDate + "&dateTo=" + tDate + "&partner=picky&curr=" + currency);
        }

        //Loop through all the entries in the flights json
        for(Map.Entry<String, JsonElement> entry: flightsJson.entrySet()){
            //Verify if the currency is Euro. Otherwise it breaks
            if(entry.getKey().equals("currency") && !entry.getValue().getAsString().equals("EUR")){
                break;
            }
            //Verify if the entry in the api results is the data
            if(entry.getKey().equals("data")){
                //Loop through the flights in the data
                for(JsonElement dataEntry: entry.getValue().getAsJsonArray()){
                    //This verifies if the airlines are always tap or ryanair not allowing other airlines
                    if(dataEntry.getAsJsonObject().get("airlines").getAsJsonArray().get(0).getAsString().equals("TP") || dataEntry.getAsJsonObject().get("airlines").getAsJsonArray().get(0).getAsString().equals("FR")) {
                        //verify if the value is not null (not common but might happen)
                        if(dataEntry.getAsJsonObject().get("price") != null) {
                            //Add the price to the arraylist of prices
                            priceFlights.add(dataEntry.getAsJsonObject().get("price").getAsDouble());
                        }
                        //If the price is equivalent to null then it doesn't add to the linkedlist
                        if (dataEntry.getAsJsonObject().get("bags_price").getAsJsonObject().get("1") != null) {
                            oneBagPrice.add(dataEntry.getAsJsonObject().get("bags_price").getAsJsonObject().get("1").getAsDouble());
                        }
                        //If the price is equivalent to null then it doesn't add to the linkedlist
                        if (dataEntry.getAsJsonObject().get("bags_price").getAsJsonObject().get("2") != null) {
                            twoBagPrice.add(dataEntry.getAsJsonObject().get("bags_price").getAsJsonObject().get("2").getAsDouble());
                        }
                    }
                    //Verify the dates and adjust properly
                    if(startDate == null && endDate == null){
                        //Convert the epochs to instant. Store the value in temp variable
                        Instant tempDateTime = Instant.ofEpochSecond(dataEntry.getAsJsonObject().get("dTime").getAsLong());
                        //Add the date to the start and end dates from instant.
                        startDate = Date.from(tempDateTime);
                        endDate = Date.from(tempDateTime);
                    }else{
                        //create temp variable to contain the date
                        Instant tempDateTime = Instant.ofEpochSecond(dataEntry.getAsJsonObject().get("dTime").getAsLong());
                        //Verify if the date is before the startDate
                        if(Date.from(tempDateTime).before(startDate)){
                            startDate = Date.from(tempDateTime);
                            //Verify if the date is after the endDate
                        }else if(Date.from(tempDateTime).after(endDate)){
                            endDate = Date.from(tempDateTime);
                        }
                    }
                }
            }
        }
        //Streams were used for simplicity and better performance. As to be an optionalDouble since it might be empty. Calculates prices average
        OptionalDouble averageFlightPrice = priceFlights.stream().mapToDouble(v -> v).average();
        //Streams were used for simplicity and better performance. As to be an optionalDouble since it might be empty. Calculates prices average
        OptionalDouble averageOneBagPrice = oneBagPrice.stream().mapToDouble(v -> v).average();
        //Streams were used for simplicity and better performance. As to be an optionalDouble since it might be empty. Calculates prices average
        OptionalDouble averageTwoBagsPrice = twoBagPrice.stream().mapToDouble(v -> v).average();

        //Add the averageFlightPrice to results json
        if(averageFlightPrice.isPresent()){
            results.addProperty("AverageFlightPrice", averageFlightPrice.getAsDouble());
        }else{
            results.addProperty("AverageFlightPrice", 0);
        }

        //Add the averageOneBagPrice to results json
        if(averageOneBagPrice.isPresent()){
            results.addProperty("OneBagPrice", averageOneBagPrice.getAsDouble());
        }else{
            results.addProperty("OneBagPrice", 0);
        }

        //Add the averageTwoBagPrice to results json
        if(averageTwoBagsPrice.isPresent()){
            results.addProperty("TwoBagsPrice", averageOneBagPrice.getAsDouble());
        }else{
            results.addProperty("TwoBagsPrice", 0);
        }

        //Add currency
        results.addProperty("currency", "EUR");

        //Add From Date
        results.addProperty("From", startDate.toString());

        //Add To Date
        results.addProperty("To", endDate.toString());

        //add record to the db
        addRecordToDb(results);

        //return the json with the results
        return results;
    }

    //Set to private since this is for internal usage only
    private void addRecordToDb(JsonObject record){
        //Might throw sqlexection so is wrapped around try
        try{
            //Send the values into the db
            this.statement.executeUpdate("insert into Records(record) values (\'" + record.toString() + "\');");
        }catch(SQLException e){
            System.out.println("Error while writing to DB");
            e.printStackTrace();
        }
    }

    //Get all the records
    public StringBuilder getAllRecords(){
        //Stringbuilder for the results
        StringBuilder entries = new StringBuilder();
        try{
            //Grab all the values from the database
            ResultSet dbEntries = this.statement.executeQuery("select * from records");
            while(dbEntries.next()){
                String line = "ID: " + dbEntries.getInt(1) + " Data: " + dbEntries.getString(2) + " Date: " + dbEntries.getDate(3);
                entries.append(line);
            }
        }catch(SQLException e){
            System.out.println("Error getting all the data");
            e.printStackTrace();
        }

        //Return the results
        return entries;

    }

    //Delete all the entries
    public void deleteAllRecords(){
        try{
            //Grab all the values from the database
            this.statement.execute("delete from Records");
        }catch(SQLException e){
            System.out.println("Error getting all the data");
            e.printStackTrace();
        }
    }

}
