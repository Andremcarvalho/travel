package com.cocus.travel.TravelApi;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import com.cocus.travel.TravelApi.TravelApiApplication;
import org.springframework.util.Assert;

import java.time.Instant;
import java.time.Period;

@SpringBootTest
class TravelApiApplicationTests {

	@Test
	void flightsForCity() {
		TravelApiApplication testClass = new TravelApiApplication();

		Assert.notNull(testClass.getAllflights("OPO"), "Json For City Ok");
	}

	@Test
	void flightsForCurrency(){
		TravelApiApplication testClass = new TravelApiApplication();

		Assert.notNull(testClass.getAllflights("OPO", "EUR"), "Json For City + Currency Ok");
	}

	@Test
	void flightsWithDate(){
		TravelApiApplication testClass = new TravelApiApplication();

		Assert.notNull(testClass.getFlightsWithDateRestrictions("OPO", "12-07-2020", "18-07-2020"), "Json For City + Dates Ok");
	}

	@Test
	void flightsWithDateAndCurrency(){
		TravelApiApplication testClass = new TravelApiApplication();

		Assert.notNull(testClass.getFlightsWithDateAndCurrencyRestrictions("OPO", "EUR", "12-07-2020", "18-07-2020"), "Json For City + Currency + Dates Ok");
	}

}
