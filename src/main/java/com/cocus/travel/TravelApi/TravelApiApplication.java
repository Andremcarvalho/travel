package com.cocus.travel.TravelApi;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@EnableAutoConfiguration
@RestController
public class TravelApiApplication {

	public static void main(String[] args) {

		SpringApplication.run(TravelApiApplication.class, args);

	}

	//Get method for flights with departure city information
	@RequestMapping(value = "/flights/{departureCity}", method = RequestMethod.GET)
		public String getAllflights(@PathVariable("departureCity") String departureCity){
			//Create new flights object
			Flights flightsController = new Flights();
			//Get the values with the desired restrictions
			JsonObject result = flightsController.getFlightsWithCity(departureCity);
			//Return the results
			return result.toString();
		}

	//Get method for flights with departure city and currency
	@RequestMapping(value = "/flights/{departureCity}/{currency}", method = RequestMethod.GET)
		public String getAllflights(@PathVariable("departureCity") String departureCity, @PathVariable("currency") String currency){
			//Create new flights object
			Flights flightsController = new Flights();
			//Get the values with the desired restrictions
			JsonObject result = flightsController.getFlightsWithCurrency(departureCity, currency);
			//Return the results
			return result.toString();
	}

	//Get method for flights with the departure city and dates
	@RequestMapping(value = "/flights/{departureCity}/{fDate}/{tDate}", method = RequestMethod.GET)
		public String getFlightsWithDateRestrictions(@PathVariable("departureCity") String departureCity, @PathVariable("fDate") String fDate, @PathVariable("tDate") String tDate){
			//Create new flights object
			Flights flightsController = new Flights();
			//Replace the - on the dates into / for respecting the desired format
			fDate = fDate.replace("-", "/");
			tDate = tDate.replace("-", "/");
			//Get the values with the desired restrictions
			JsonObject result = flightsController.getFlightsWithTime(departureCity, fDate, tDate);
			//Return the results
			return result.toString();
		}

	//Get method for flights with departure, currency and dates
	@RequestMapping(value = "/flights/{departureCity}/{currency}/{fDate}/{tDate}", method = RequestMethod.GET)
		public String getFlightsWithDateAndCurrencyRestrictions(@PathVariable("departureCity") String departureCity,@PathVariable("currency") String currency, @PathVariable("fDate") String fDate, @PathVariable("tDate") String tDate){
			//Create new flights object
			Flights flightsController = new Flights();
			//Replace the - on the dates into / for respecting the desired format
			fDate = fDate.replace("-", "/");
			tDate = tDate.replace("-", "/");
			//Get the values with the desired restrictions
			JsonObject result = flightsController.getFlightsWithCurrencyAndDate(departureCity, currency, fDate, tDate);
			//Return the results
			return result.toString();
		}

	//Get method for all DB entries
	@RequestMapping(value = "/entries", method = RequestMethod.GET)
	public String getEntries(){
		//Create new flights object
		Flights flightsController = new Flights();
		//Get the entries
		StringBuilder result = flightsController.getAllRecords();
		//Return the results
		return result.toString();
	}

	//Get method for cleaning the db
	@RequestMapping(value = "/clean", method = RequestMethod.GET)
	public String deleteEntries(){
		//Create new flights object
		Flights flightsController = new Flights();
		//Get the entries
		flightsController.deleteAllRecords();
		//Return the results
		return "Done";
	}

}
